import json
import os
import requests
from flask import Flask ,request
shutdown = False
app = Flask(__name__)
@app.teardown_request
def teardown(exception):
    print(exception)
    global shutdown
    if shutdown:
        os._exit(0)
@app.after_request
def treat_as_plain_text(response):
    response.headers["Content-Type"] = "application/json; charset=utf-8"
    return response

@app.route("/",methods=['POST'])
def main():
    try:
        data = json.loads(request.get_data())
        x = requests.post(f"https://{data['domain']}.zportal.nl/api/v3/oauth", data={'username': data['username'], 'password': data['password'], 'client_id': 'OAuthPage', 'redirect_uri': '/main/',
                'scope': '', 'state': '4E252A', 'response_type': 'code', 'tenant':  data['domain']},allow_redirects=False)
        url = x.headers['Location']
        start = url.find("code=")+5
        token = url[start:url.find("&",start)]
        return {"token":token}
    except Exception as e:
        print(e)
        return {"error":"missing info"}
    
if __name__ == '__main__':
    app.run(host="0.0.0.0", port=6000,debug=False)
else:
    def start():
        return app
